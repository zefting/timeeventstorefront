import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  cartProducts = [];
  productsByNumber = [];
  constructor(private cartService: CartService) { }
  getCurrentCart(){
    this.cartProducts = this.cartService.CurrentCart;
    console.log("InitProducts", this.cartProducts);
  }
  countProductsInCart(){

    for (let product of this.cartProducts)  {

      if(!this.productsByNumber.includes(product)) {
        product["numberofproducts"] = 1;
        this.productsByNumber.push(product)
        console.log("new added", this.productsByNumber);
        }
        if(this.productsByNumber.includes(product)) {
          product.numberofproducts++;
          console.log("added one", this.productsByNumber);
        }
      }
    }

     // counts[x] = (counts[x] || 0)+1; }
      //);
   // console.log(counts);

  removeCartItem(id): void {
    this.cartProducts = this.productsByNumber.filter(item => item.id !== id);
    console.log("CartAfterDelete", this.cartProducts);
  }
  ngOnInit(): void {
    this.getCurrentCart();
    this.countProductsInCart();
  }

}
