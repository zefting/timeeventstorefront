
import { Component, OnInit } from '@angular/core';

import { Observable, timer, NEVER, BehaviorSubject, fromEvent, of } from 'rxjs';
import { map, tap, takeWhile, share, startWith, switchMap, filter } from 'rxjs/operators';
import { CartService } from '../cart.service';


@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {


  constructor(private cartService: CartService) { }
  productCards:any;

  nextSale:number;
  message:any;
  socialMediaLiveUrl: string;
  addToCart(item){
    this.cartService.CurrentCart.push(item);
    console.log(item);
  }
  // get campagin duration
  campaginDuration(){
    //calulate time remaining in minutes

    var starttime:any = new Date(2020,11,12, 10,10).getMinutes();
    console.log("Start time",starttime);
    var endtime:any = new Date(2020,11,12, 10,20).getMinutes();
    console.log("End time", endtime);

    let timeRemaining = endtime - starttime;
    console.log("Time remainging",timeRemaining);
    return timeRemaining;



  }

  countdownFunction(minutes:number){
     // countdown functionality https://stackblitz.com/edit/rxjs-rajp6s?file=index.html
     const toggle$ = new BehaviorSubject(true);
     const K = 1000;
     const INTERVAL = K;
     const MINUTES = this.campaginDuration();
     const TIME = MINUTES * K * 60;
     let current: number;
     let time = TIME;

     const toMinutesDisplay = (ms:number) => Math.floor(ms / K / 60);
     const toSecoundsDisplay = (ms:number) => Math.floor(ms / K) % 60;

     const toSecoundsDisplayString = (ms:number) => {
       const seconds  = toSecoundsDisplay(ms);
       return seconds  < 10 ? `0${seconds}` : seconds.toString();
     }
     const currentSecounds = () => time / INTERVAL;
     const toMs = (t:number) => t * INTERVAL;
     const toRemainingSecounds = (t:number) => currentSecounds() -t;

     const toRemainingSecounds$ = toggle$.pipe(
       switchMap((running:boolean) => (running ? timer(0, INTERVAL) : NEVER)),
       map(toRemainingSecounds),
       takeWhile(t => t >= 0),
     );

     const ms$ = toRemainingSecounds$.pipe(
       map(toMs),
       tap(t=> current = t)
     );

     const minutes$ = ms$.pipe(
       map(toMinutesDisplay),
       map(s => s.toString()),
       startWith(toMinutesDisplay(time).toString())
     );
     const seconds$ = ms$.pipe(
       map(toSecoundsDisplayString),
       startWith(toSecoundsDisplayString(time).toString())
     );

     // update DOM
     const minutesElement = document.querySelector('.minutes');
     const secoundsElement = document.querySelector('.secounds');
     const toggleElement = document.querySelector('.timer');

     updateDom(minutes$, minutesElement);
     updateDom(seconds$, secoundsElement);
    /*
    // opdater, så den henter nye produkter
    timer$.subscribe({
    complete: () => updateDom(of('Take a break!'), toggleElement)
});
    */

     function updateDom(source$: Observable<string>, element: Element) {
       source$.subscribe((value) => element.innerHTML = value);
     }

  }
  ngOnInit(): void {
    this.countdownFunction(2);
    this.campaginDuration()
    this.socialMediaLiveUrl = "https://www.facebook.com/facebook/videos/10153231379946729/";
    this.nextSale = 10;
    this.productCards =
    [
      {
        "id":1,
        "title":"Ipsum",
        "desc": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nunc dui, rutrum in cursus non, aliquam sit amet ligula. Donec nec rhoncus felis.",
        "price": 22.22,
        "url":"./assets/img/p1.jpg",
        "type": "Blue"
      },
      {
        "id":2,
        "title":"Ipsum",
        "desc": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nunc dui, rutrum in cursus non, aliquam sit amet ligula. Donec nec rhoncus felis.",
        "price": 22.22,
        "url":"./assets/img/p1.jpg",
        "type": "Blue"
      },
      {
        "id":3,
        "title":"Ipsum",
        "desc": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nunc dui, rutrum in cursus non, aliquam sit amet ligula. Donec nec rhoncus felis.",
        "price": 22.22,
        "url":"./assets/img/p1.jpg",
        "type": "Blue"
      },
      {
        "id":4,
        "title":"Ipsum",
        "desc": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nunc dui, rutrum in cursus non, aliquam sit amet ligula. Donec nec rhoncus felis.",
        "price": 22.22,
        "url":"./assets/img/p1.jpg",
        "type": "Blue"
      },
      {
        "id":5,
        "title":"Ipsum",
        "desc": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nunc dui, rutrum in cursus non, aliquam sit amet ligula. Donec nec rhoncus felis.",
        "price": 22.22,
        "url":"./assets/img/p1.jpg",
        "type": "Blue"
      },
      {
        "id":6,
        "title":"Ipsum",
        "desc": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nunc dui, rutrum in cursus non, aliquam sit amet ligula. Donec nec rhoncus felis.",
        "price": 22.22,
        "url":"./assets/img/p1.jpg",
        "type": "Blue"
      }
    ]
  }

}
